# w3id

This directory defines the redirections under which ListDBOnto will be available.

ListDBOnto will use the [w3id service](https://github.com/perma-id/w3id.org) and implement corresponding identifier to reference its classes, properties and indviduals. See also [_Permanent Identifiers for the Web_](https://w3id.org/).

The identifiers can also be used to refer to documentation and other resources.

The future identifiers will start with [https://www.w3id.org/listdb/onto#](https://www.w3id.org/listdb/onto#).
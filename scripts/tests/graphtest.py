from rdflib import Graph
from pathlib import Path

ListDBOnto = Path(__file__).parent.parent.parent/'ListDB-Onto.owl'

# def load_file(filepath, file_format):
#     g = rdflib.Graph()
#     g.parse(filepath, format=file_format)
#     return g

def parse_onto(onto_purl, onto_format):
    g = Graph()
    g.parse(onto_purl, format=onto_format)
    print(f"Graph g has {len(g)} statements.")
    assert len(g) > 0, f'Error: No triples found in {onto_purl}.'
    assert g, f'Error: {onto_purl} is not a graph'

parse_onto(str(ListDBOnto), onto_format="xml")

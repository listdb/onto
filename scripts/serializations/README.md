# Update Serializations

This directory contains a script to create different serializations of ListDBOnto. Their tagged versions will be the targets of redirects of the versioned w3ids.

**Example redirect:**

`https://w3id.org/listdb/onto/0.0.1/ontology.ttl`

will be redirected to

`https://gitlab.com/listdb/onto/-/raw/0.0.1/serializations/ontology.ttl`

The script employs rdflib and needs to be run manually when commiting changes in [ListDB-Onto.owl](ListDB-Onto.owl).

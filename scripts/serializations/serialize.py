from rdflib import Graph
from pathlib import Path
from rdflib import plugin

ListDBOnto = Path(__file__).parent.parent.parent/'ListDB-Onto.owl'
xml = Path(__file__).parent.parent.parent/'serializations'/ 'ontology.xml'
ttl = Path(__file__).parent.parent.parent/'serializations'/ 'ontology.ttl'
json = Path(__file__).parent.parent.parent/'serializations'/ 'ontology.jsonld'
nt = Path(__file__).parent.parent.parent/'serializations'/ 'ontology.nt'
n3 = Path(__file__).parent.parent.parent/'serializations'/ 'ontology.n3'
trig = Path(__file__).parent.parent.parent/'serializations'/ 'ontology.trig'
trix = Path(__file__).parent.parent.parent/'serializations'/ 'ontology.trix'
# nquads = Path(__file__).parent.parent.parent/'serializations'/ 'ontology.nquads'
#print(ttl)


def parse_onto(onto_purl, onto_format):
    g = Graph()
    g.parse(onto_purl, format=onto_format)
    print(f"Graph g has {len(g)} statements.")
    assert len(g) > 0, f'Error: No triples found in {onto_purl}.'
    assert g, f'Error: {onto_purl} is not a graph'
    g.serialize(destination=str(xml), format = "xml")
    g.serialize(destination=str(ttl), format = "ttl")
    g.serialize(destination=str(json), format = "json-ld")
    g.serialize(destination=str(nt), format = "nt")
    g.serialize(destination=str(n3), format = "n3")
    g.serialize(destination=str(trig), format = "trig")
    # g.serialize(destination=str(nquads), format = "nquads")

parse_onto(str(ListDBOnto), onto_format="xml")

# Scripts

This directory contains scripts for different purposes.

## [scripts/serializations](scripts/serializations)

Contains a python script applying [rdflib](https://rdflib.readthedocs.io/en/stable/#) to create different serializations for ListDB Onto that will serve as targets for redirects from w3id to versioned serializations of ListDBOnto. It needs to be triggered locally after changes to [ListDBOnto.owl](ListDBOnto.owl) have been made and before these are pushed to upstream.

## [scripts/tests](scripts/tests)

Contains a python script applying [rdflib](https://rdflib.readthedocs.io/en/stable/#) to test whether ListDBOnto can be parsed as a graph. This test is run each time a change on the ontology is commited to a merge request. Alternatively, the test can be triggered manually in the CI/CD menu of the remote repository or triggered locally. It helps to ensure that the ontology on the main and develop branch are in working order at all times.

## [scripts/widoco](scripts/widoco)

Contains scripts to customise sections of the [documentation of ListDBOnto](https://w3id.org/listdb/onto). This documentation is auto-generated from the ontology file with [Widoco](https://github.com/dgarijo/Widoco).

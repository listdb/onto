# Contribution Guidelines

## External users
If you have a comment or suggestion for changes, create an issue in our [issue tracker](https://gitlab.com/listdb/onto/-/issues).

If you want to solve an issue please consider the following: Each issue should have a corresonding merge request which is marked as a draft as long as contributions are being made to it. Both issue and merge request should relate to a single branch containig all changes that are required to solve an issue.

If you make changes to the ontology please make sure to:

- check for correct syntax (e.g. by loading with Protègè),
- check for coherence and consistency in Protègè with Hermit reasoner,
- update the documentation.

If you are done working on an issue, mark your merge request as ready and assign a reviewer (Susanne Arndt or Maximilian Bäumler). The reviewer will go through your merge request, give some feedback or, if none is needed, merge your merge request. Please consider the feedback in your merge request.

You can also fork the project and commit your changes via pull request.

## Merge request policy (admins)

The `develop` branch is the default protected branch of the project. By default, feature branches will branch from it, e.g. when creating a merge request at an issue.

Merge requests should ask to be merge a feature branch into the `develop` branch. It should always contain the latest changes.

Tags and releases will be branched from `main`. In order to get the latest features to `main`, `main` should be rebased from develop with `git rebase develop -i`.

Each time work on the ontology is performed, the serializations at [serializations](serializations) should be updated before merging. The required scripts are located at [scripts/serializations](scripts/serializations).

Updates to the ontology should also be adressed by the documentation and corresponding visualization.

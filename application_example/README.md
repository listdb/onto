# Application examples (WIP)

This directory contains an exemplary metadata description for a traffic observation data set. The three files are different serializations containing the same information.

## File 1: [application_example/20210512_1452_Fic_Hec_4W_d_1.txt](application_example/20210512_1452_Fic_Hec_4W_d_1.txt)

This text file is the original exemplary metadata file which demonstrates the structure of metadata files that are actually included in the ListDB data base.

## File 2: [application_example/20210512_1452_Fic_Hec_4W_d_1.owl](application_example/20210512_1452_Fic_Hec_4W_d_1.owl)

This file is an RDF/XML serialization of an RDF-based representation of the metadata information in File 1. It applies the terms of ListDBOnto and has been created manually.

## File 3: [application_example/20210512_1452_Fic_Hec_4W_d_1.json](application_example/20210512_1452_Fic_Hec_4W_d_1.json)

This file is a JSON-LD serialization of an RDF-based representation of the metadata information in File 1. It can be created with an open RDF-converter [1] from File 2.

## File 4: [application_example/20210512_1452_Fic_Hec_4W_d_1_compacted.json](application_example/20210512_1452_Fic_Hec_4W_d_1_compacted.json)

This is a compacted version of file 3. It has been created at the JSON-LD Playground [2] from file 3, applying the context definitions specified in [application_example/context-definition.json](application_example/context-definition.json).

## Resources

[1] Easy RDF Converter. URL: [https://www.easyrdf.org/converter](https://www.easyrdf.org/converter).
[2] JSON-LD Playground. URL: [https://json-ld.org/playground/](https://json-ld.org/playground/).

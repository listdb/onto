# Widoco customization

This directory contains elements to customize an auto-generated documentation of ListDBOnto. There are:

* text snippets that will be used in a documentation generated from the ontology by Widoco
* configuration data for Widoco

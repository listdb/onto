The ListDB Ontology draft is referring back to the [Data Catalogue Vocabulary (DCAT, Version 2)](https://www.w3.org/TR/vocab-dcat-2/). DCAT is "an RDF vocabulary designed to facilitate interoperability between data catalogs published on the Web" (Albertoni et al. 2020). It "enables a publisher to describe datasets and data services in a catalog using a standard model" (Albertoni et al. 2020).

"A dataset in DCAT is defined as a 'collection of data, published or curated by a single agent, and available for access or download in one or more serializations or formats'. A dataset is a conceptual entity, and can be represented by one or more distributions that serialize the dataset for transfer. Distributions of a dataset can be provided via data services" (Albertoni et al. 2020).

ListDB basically is a data catalog where information about data sets is stored and which points to the distributions and files belonging to a data set. Each data set in ListDB is centered around a single traffic observation. The basic model of ListDB Ontology is therefore depicted as follows:

![Visualization of Traffic Observation Data Sets and Relation of ListDB Ontology to DCAT 2](https://gitlab.com/listdb/onto/-/raw/0.0.1/visualizations/traffic_observation_dataset.png)

Each traffic observation data set results from a video-based traffic observation. It has at least one metadata file, at least one original video and at least one file describing the intrinsic camera paramters of the camera the video-based traffic observation has been conducted with. In addtion the traffic observation dataset can optionally contain a file describing extrinsic camera parameters, a calibrated video, and an analysis file.

The metadata file provides information about the traffic observation like

- information about the location of the recording,
- temporal infomation,
- information about the environment,
- organizational information, or
- information about measurements.

![Visualization of Class 'metadata file' and metadata properties for metadata file instances](https://gitlab.com/listdb/onto/-/raw/0.0.1/visualizations/metadata_file.png)

The metadata file description refers to classes that define subclasses relevant for the description of traffic observations. The next visualization gives some examples of subclasses of the class 'Location':

![Visualization for class 'Location' and some examples for subclasses](https://gitlab.com/listdb/onto/-/raw/0.0.1/visualizations/Location_class.png)

The description of an instance of class 'metadata file' uses specific subproperties and a range of defined values:

![Visualization of an instance of class 'metadata file' with some example relations and values](https://gitlab.com/listdb/onto/-/raw/0.0.1/visualizations/instance_metadata_file.png)

A JSON-LD representation of a metadata file example refers to the terms defined in ListDB Ontology (and other semantic resources) via its identifiers. The example also contains the element for the data set the metadata file belongs to:

```JSON-LD
{
    "@context": {
      "owl": "http://www.w3.org/2002/07/owl#",
      "dcterms": "http://purl.org/dc/terms/",
      "geo": "http://www.w3.org/2003/01/geo/wgs84_pos#",
      "example": "http://www.semanticweb.org/new/ontologies/2022/3/5/",
      "skos": "http://www.w3.org/2004/02/skos/core#",
      "listdb": "https://w3id.org/listdb/onto#",
      "wikidata": "http://www.wikidata.org/entity/",
      "dcat": "http://www.w3.org/ns/dcat#"
    },
    "@graph": [
      {
        "@type": "owl:Axiom",
        "owl:annotatedProperty": {
          "@id": "skos:prefLabel"
        },
        "owl:annotatedSource": {
          "@id": "listdb:I0000000118"
        },
        "owl:annotatedTarget": "02_Static-01_Urban-01_Node-01_4W-01_Dry-20210512",
        "skos:editorialNote": {
          "@language": "en",
          "@value": "The label of the traffic observation is analogue to the path of the folder where the outputs of the traffic observation will be located. The structure is Type_Location_Shape_Geometry_RoadCondition_DateofRecording.\n\nThis is probably not the optimal way of identifying an instance of a video-based traffic observation, since the folder may contain videos from several such video-based traffic observations, employing different tools and parameters."
        }
      },
      {
        "@id": "http://edamontology.org/format_3997",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "http://edamontology.org/has_format",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "http://purl.obolibrary.org/obo/RO_0002234",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "dcterms:spatial",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "http://w3id.org/nfdi4ing/metadata4ing#createdWithTool",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "http://w3id.org/nfdi4ing/metadata4ing#hasEmployedTool",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "http://www.semanticweb.org/new/ontologies/2022/7/1/untitled-ontology-569",
        "@type": "owl:Ontology"
      },
      {
        "@id": "geowgs84:location",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "skos:editorialNote",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "skos:prefLabel",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "http://www.w3.org/ns/prov#qualifiedAssociation",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "wikidata:P281",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "wikidata:P669",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "listdb:AP0000000064",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "listdb:AP0000000075",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "listdb:AP0000000151",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "listdb:AP0000000161",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "listdb:AP0000000162",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "listdb:AP0000000163",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "listdb:C0000000001",
        "@type": "owl:Class"
      },
      {
        "@id": "listdb:C0000000003",
        "@type": "owl:Class"
      },
      {
        "@id": "listdb:C0000000007",
        "@type": "owl:Class"
      },
      {
        "@id": "listdb:C0000000008",
        "@type": "owl:Class"
      },
      {
        "@id": "listdb:C0000000061",
        "@type": "owl:Class"
      },
      {
        "@id": "listdb:C0000000062",
        "@type": "owl:Class"
      },
      {
        "@id": "listdb:C0000000202",
        "@type": "owl:Class"
      },
      {
        "@id": "listdb:DP0000000027",
        "@type": "owl:DatatypeProperty"
      },
      {
        "@id": "listdb:DP0000000028",
        "@type": "owl:DatatypeProperty"
      },
      {
        "@id": "listdb:DP0000000029",
        "@type": "owl:DatatypeProperty"
      },
      {
        "@id": "listdb:DP0000000187",
        "@type": "owl:DatatypeProperty"
      },
      {
        "@id": "listdb:E0000000213",
        "@type": "owl:AnnotationProperty"
      },
      {
        "@id": "listdb:E0000000216",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:E0000000217",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:E0000000223",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:E0000000224",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:E0000000226",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:E0000000230",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:E0000000231",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:E0000000232",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:E0000000235",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:E0000000236",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:E0000000237",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:E0000000251",
        "@type": [
          "owl:NamedIndividual",
          "listdb:C0000000061"
        ],
        "skos:prefLabel": {
          "@language": "en",
          "@value": "20210512_1452_Fic_Hec_4W_d_1"
        },
        "listdb:E0000000252": [
          {
            "@id": "listdb:I0000000002"
          },
          {
            "@id": "listdb:I0000000004"
          },
          {
            "@id": "listdb:I0000000005"
          },
          {
            "@id": "listdb:I0000000016"
          }
        ]
      },
      {
        "@id": "listdb:E0000000252",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:I0000000001",
        "@type": "owl:NamedIndividual",
        "listdb:OP0000000001": [
          {
            "@id": "listdb:I0000000004"
          },
          {
            "@id": "listdb:I0000000005"
          }
        ]
      },
      {
        "@id": "listdb:I0000000002",
        "@type": [
          "owl:NamedIndividual",
          "listdb:C0000000001",
          "listdb:C0000000003"
        ],
        "http://edamontology.org/has_format": {
          "@id": "http://edamontology.org/format_3997"
        },
        "http://w3id.org/nfdi4ing/metadata4ing#createdWithTool": {
          "@id": "listdb:I0000000001"
        },
        "skos:prefLabel": "20210512_1452_Fic_Hec_4W_d_1_1_org.mp4"
      },
      {
        "@id": "listdb:I0000000004",
        "@type": [
          "owl:NamedIndividual",
          "listdb:C0000000007"
        ],
        "skos:prefLabel": {
          "@language": "en",
          "@value": "intrinsic camera parameters file of traffic observation data set 20210512_1452_Fic_Hec_4W_d_1"
        }
      },
      {
        "@id": "listdb:I0000000005",
        "@type": [
          "owl:NamedIndividual",
          "listdb:C0000000008"
        ],
        "skos:prefLabel": {
          "@language": "en",
          "@value": "extrinsic camera parameters of 20210512_1452_Fic_Hec_4W_d_1"
        }
      },
      {
        "@id": "listdb:I0000000009",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000016",
        "@type": [
          "owl:NamedIndividual",
          "listdb:C0000000062"
        ],
        "dcterms:spatial": {
          "@id": "_:b1"
        },
        "geowgs84:location": {
          "@id": "listdb:E0000000226"
        },
        "skos:prefLabel": {
          "@language": "en",
          "@value": "20210512_1452_Fic_Hec_4W_d_1.txt"
        },
        "wikidata:P281": "01097",
        "wikidata:P669": [
          {
            "@id": "listdb:I0000000046"
          },
          {
            "@language": "de",
            "@value": "Fichtenstrasse"
          },
          {
            "@language": "de",
            "@value": "Hechstrasse"
          }
        ],
        "listdb:AP0000000064": {
          "@language": "en",
          "@value": "GoPro mounted on drone. Videos were processed afterwards before saving."
        },
        "listdb:AP0000000075": "Dresden",
        "listdb:AP0000000151": {
          "@id": "listdb:I0000000046"
        },
        "listdb:AP0000000161": 1,
        "listdb:AP0000000162": "GoPro Hero Session4",
        "listdb:AP0000000163": "C3141324515055",
        "listdb:DP0000000027": {
          "@language": "de",
          "@value": "LKT"
        },
        "listdb:DP0000000028": "Test",
        "listdb:DP0000000029": 2,
        "listdb:DP0000000187": "20210512_1452",
        "listdb:E0000000213": {
          "@id": "listdb:I0000000206"
        },
        "listdb:E0000000216": {
          "@id": "listdb:E0000000217"
        },
        "listdb:E0000000223": {
          "@id": "listdb:E0000000224"
        },
        "listdb:E0000000230": {
          "@id": "listdb:E0000000231"
        },
        "listdb:E0000000232": {
          "@id": "listdb:E0000000235"
        },
        "listdb:E0000000236": {
          "@id": "listdb:E0000000237"
        },
        "listdb:OP0000000018": {
          "@id": "listdb:I0000000024"
        },
        "listdb:OP0000000038": {
          "@id": "listdb:I0000000034"
        },
        "listdb:OP0000000042": {
          "@id": "listdb:I0000000044"
        },
        "listdb:OP0000000049": {
          "@id": "listdb:I0000000048"
        },
        "listdb:OP0000000056": {
          "@id": "listdb:I0000000054"
        },
        "listdb:OP0000000060": {
          "@id": "listdb:I0000000058"
        },
        "listdb:OP0000000073": {
          "@id": "listdb:I0000000067"
        },
        "listdb:OP0000000085": {
          "@id": "listdb:I0000000081"
        },
        "listdb:OP0000000118": {
          "@id": "listdb:I0000000088"
        },
        "listdb:OP0000000119": {
          "@id": "listdb:I0000000111"
        },
        "listdb:OP0000000124": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000125": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000127": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000129": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000165": {
          "@id": "listdb:I0000000155"
        },
        "listdb:OP0000000166": {
          "@id": "listdb:I0000000160"
        },
        "listdb:OP0000000167": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000168": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000169": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000170": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000171": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000172": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000173": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000174": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000175": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000176": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000177": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000178": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000179": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000180": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000181": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000182": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000183": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000184": {
          "@id": "listdb:I0000000122"
        },
        "listdb:OP0000000185": {
          "@id": "listdb:I0000000149"
        },
        "listdb:OP0000000190": {
          "@id": "listdb:I0000000036"
        },
        "listdb:OP0000000194": {
          "@id": "listdb:I0000000046"
        },
        "listdb:OP0000000196": {
          "@id": "listdb:I0000000036"
        },
        "listdb:OP0000000197": {
          "@id": "listdb:I0000000036"
        },
        "listdb:OP0000000199": [
          {
            "@id": "listdb:I0000000200"
          },
          {
            "@id": "_:b0"
          }
        ],
        "listdb:OP0000000201": {
          "@id": "listdb:I0000000202"
        }
      },
      {
        "@id": "listdb:I0000000024",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000034",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000036",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000044",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000046",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000048",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000054",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000058",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000067",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000081",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000088",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000111",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000118",
        "@type": [
          "owl:NamedIndividual",
          "listdb:C0000000202"
        ],
        "http://purl.obolibrary.org/obo/RO_0002234": {
          "@id": "listdb:E0000000251"
        },
        "http://w3id.org/nfdi4ing/metadata4ing#hasEmployedTool": {
          "@id": "listdb:I0000000001"
        },
        "skos:prefLabel": "02_Static-01_Urban-01_Node-01_4W-01_Dry-20210512",
        "http://www.w3.org/ns/prov#qualifiedAssociation": {
          "@id": "listdb:I0000000009"
        }
      },
      {
        "@id": "listdb:I0000000122",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000149",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000155",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000160",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000200",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:I0000000202",
        "@type": "owl:NamedIndividual"
      },
      {
        "@id": "listdb:OP0000000001",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000018",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000038",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000042",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000049",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000056",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000060",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000073",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000085",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000118",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000119",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000124",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000125",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000127",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000129",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000165",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000166",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000167",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000168",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000169",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000170",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000171",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000172",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000173",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000174",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000175",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000176",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000177",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000178",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000179",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000180",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000181",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000182",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000183",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000184",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000185",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000190",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000194",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000196",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000197",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000199",
        "@type": "owl:ObjectProperty"
      },
      {
        "@id": "listdb:OP0000000201",
        "@type": "owl:ObjectProperty"
      }
    ]
  }
```